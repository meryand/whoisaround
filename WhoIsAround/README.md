## Synopsis

Itercom Test

Question 3.- 

We have some customer records in a text file (customers.json) -- one customer per line, JSON-encoded. 
We want to invite any customer within 100km of our Dublinoffice for some food and drinks on us. 
Write a program that will read the full list of customers and output the names and user ids of matching customers (within100km), sorted by User ID (ascending).

## Code

I have created a folder structure to allocate the different classes.
    
    - Helper: Location and JSONParser 
    
    * MVVM Architecture *
    - Model: Customer
    - ViewController: CustomerListViewController and CustomerListViewCell
    - ViewModel: CustomerListViewModel
    - DataController: CustomerListDataController

    - Storyboards
    - Resources: Images and JSON files

As you can see there area a MVVM (Model-View-ViewModel) structure where the code has been separated following that architecture pattern. 

The Helper folder contains those classes needed to parser the JSON and get the Great Circle Distance to calculate the distance of the customer.

## Tests

In the folder WhoIsAroundTest there are couple of classes where you can find the unit tests done.

## Simulater

On the simulator you can see a simple app which shows the list of customers within 100KM and sort by UserID. Tapping on any user info you could see a "draft" invitation to those who appears near.