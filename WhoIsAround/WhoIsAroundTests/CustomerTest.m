//
//  CustomerTest.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 12/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Customer.h"

@interface CustomerTest : XCTestCase

@end

@implementation CustomerTest {
    Customer *customer1;
    Customer *customer2;
    Customer *customer3;
    NSArray *customersArray;
}

- (void)setUp {
    [super setUp];
    customersArray = @[ @{
                            @"latitude": @"52.986375",
                            @"user_id": @12,
                            @"name": @"Christina McArdle",
                            @"longitude": @"-6.043701"
                            }, @{
                            @"latitude": @"51.92893",
                            @"user_id": @1,
                            @"name": @"Alice Cahill",
                            @"longitude": @"-10.27699"
                            }, @{
                            @"latitude": @"52.833502",
                            @"user_id": @25,
                            @"name": @"David Behan",
                            @"longitude": @"-8.522366"
                            }];
    
    customer1 = [[Customer alloc] initWithDictionary:customersArray[0]];
    customer2 = [[Customer alloc] initWithDictionary:customersArray[1]];
    customer3 = [[Customer alloc] initWithDictionary:customersArray[2]];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testACustomerModel_ShouldNotBeNil {
    XCTAssertNotNil(customer1);
    XCTAssertNotNil(customer2);
    XCTAssertNotNil(customer3);
}

- (void)testACustomerModel_ShouldHaveValuesForTheProperties {
    XCTAssertNotNil(customer1.name);
    XCTAssertNotNil(customer1.userID);
    XCTAssertNotNil(customer1.latitude);
    XCTAssertNotNil(customer1.longitude);
}

- (void)testACustomerModel_ShouldHaveCorrectValuesForTheProperties {
    XCTAssertEqualObjects(customer1.name, customersArray[0][@"name"]);
    XCTAssertEqualObjects(customer1.userID, customersArray[0][@"user_id"]);
    XCTAssertEqualObjects(customer1.latitude, customersArray[0][@"latitude"]);
    XCTAssertEqualObjects(customer1.longitude, customersArray[0][@"longitude"]);
}

@end
