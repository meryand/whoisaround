//
//  CustomerListDataControllerTest.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 12/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CustomerListDataController.h"
#import "Customer.h"
#import "JSONParser.h"

@interface CustomerListDataController (Tests)
- (NSArray *)sortArrayAscending:(NSArray *)customers byKey:(NSString *)key;
@end

@interface CustomerListDataControllerTest : XCTestCase
@end

@implementation CustomerListDataControllerTest {
    CustomerListDataController *dataController;
    NSArray *input;
    NSArray *output;
    NSArray *customersArray;
}

- (void)setUp {
    [super setUp];
    dataController = [CustomerListDataController new];
    
    customersArray = @[ @{
                            @"latitude": @"51.92893",
                            @"user_id": @1,
                            @"name": @"Alice Cahill",
                            @"longitude": @"-10.27699",
                            @"distance" : @"313.097511"
                            } ,@{
                            @"latitude": @"52.986375",
                            @"user_id": @12,
                            @"name": @"Christina McArdle",
                            @"longitude": @"-6.043701",
                            @"distance" : @"41.676839"
                            }, @{
                            @"latitude": @"52.833502",
                            @"user_id": @25,
                            @"name": @"David Behan",
                            @"longitude": @"-8.522366",
                            @"distance" : @"161.216743"
                            }];
    
    Customer *customer1 = [[Customer alloc] initWithDictionary:customersArray[0]];
    Customer *customer2 = [[Customer alloc] initWithDictionary:customersArray[1]];
    Customer *customer3 = [[Customer alloc] initWithDictionary:customersArray[2]];
    
    input = @[customer1, customer2, customer3];
    output = [dataController customersDataFromFile:@"customersTest"];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testCustomersArray_ShouldNotBeNil {
    XCTAssertNotNil(output);
}

- (void)testCustomersArray_ShouldHaveTheCorrectNumberOfCustomerWithin100Km {
    XCTAssertEqual(output.count, 1);
}

- (void)testCustomersArray_ShouldHaveCustomerDistanceLessOrEqualThan100 {
    Customer *customer = output[0];
    XCTAssertLessThanOrEqual(customer.distance, 100);
}

- (void)testCustomersArray_ShouldHaveTheSameCustomerDistanceThanTheJSON {
    Customer *customer = output[0];
    double distanceInput = [(NSString *)customersArray[1][@"distance"] doubleValue];
    NSString *roundInputDistance = [NSString stringWithFormat:@"%.6f",distanceInput];
    NSString *roundOutputDistance = [NSString stringWithFormat:@"%.6f",customer.distance];
    XCTAssertEqualObjects(roundOutputDistance, roundInputDistance);
    XCTAssertEqual(roundOutputDistance.doubleValue, roundInputDistance.doubleValue);
}

- (void)testCustomersArray_ShouldHaveTheCorrectCustomerValuesWithin100Km {
    Customer *customer = output[0];
    XCTAssertEqualObjects(customer.name, customersArray[1][@"name"]);
}

- (void)testCustomersArray_ShouldHaveTheCorrectSortOrderByUserID {
    NSArray *customers = [JSONParser dataFromJsonForFileName:@"customersTest"];
    NSArray *customersSorted = [dataController sortArrayAscending:customers byKey:@"userID"];
    XCTAssertEqual(customersArray.count, customersSorted.count);
    XCTAssertEqualObjects(customersArray[0][@"user_id"], [(Customer *)customersSorted[0] userID]);
    XCTAssertEqualObjects(customersArray[1][@"user_id"], [(Customer *)customersSorted[1] userID]);
    XCTAssertEqualObjects(customersArray[2][@"user_id"], [(Customer *)customersSorted[2] userID]);
}

@end
