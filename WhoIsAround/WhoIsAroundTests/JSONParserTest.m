//
//  JSONParserTest.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 12/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "JSONParser.h"
#import "Customer.h"

@interface JSONParserTest : XCTestCase

@end

@implementation JSONParserTest {
    NSArray *input;
    NSArray *ouput;
}

- (void)setUp {
    [super setUp];
    NSArray *customersArray = @[ @{
                                     @"latitude": @"52.986375",
                                     @"user_id": @12,
                                     @"name": @"Christina McArdle",
                                     @"longitude": @"-6.043701"
                                     }, @{
                                     @"latitude": @"51.92893",
                                     @"user_id": @1,
                                     @"name": @"Alice Cahill",
                                     @"longitude": @"-10.27699"
                                     }, @{
                                     @"latitude": @"52.833502",
                                     @"user_id": @25,
                                     @"name": @"David Behan",
                                     @"longitude": @"-8.522366"
                                     }];
    
    Customer *customer1 = [[Customer alloc] initWithDictionary:customersArray[0]];
    Customer *customer2 = [[Customer alloc] initWithDictionary:customersArray[1]];
    Customer *customer3 = [[Customer alloc] initWithDictionary:customersArray[2]];
    
    input = @[customer1, customer2, customer3];
    
    ouput = [JSONParser dataFromJsonForFileName:@"customersTest"];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testParserJson_ShouldHaveTheSameNumberOfCustomer {
    XCTAssertEqual(input.count, ouput.count);
}

- (void)testParserJson_ShouldHaveTheSameCustomersName {
    XCTAssertEqualObjects([input[0] name], [ouput[0] name]);
    XCTAssertEqualObjects([input[1] name], [ouput[1] name]);
    XCTAssertEqualObjects([input[2] name], [ouput[2] name]);
}

- (void)testParserJson_ShouldHaveTheSameCustomersID {
    XCTAssertEqualObjects([input[0] userID], [ouput[0] userID]);
    XCTAssertEqualObjects([input[1] userID], [ouput[1] userID]);
    XCTAssertEqualObjects([input[2] userID], [ouput[2] userID]);
}


@end
