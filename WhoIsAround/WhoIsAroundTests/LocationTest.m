//
//  LocationTest.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 12/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Location.h"

@interface LocationTest : XCTestCase

@end

@implementation LocationTest {
    double latitude;
    double longitude;
    double greatCircleDistanceOutput;
}

- (void)setUp {
    [super setUp];
    latitude = 52.986375;
    longitude = -6.043701;
    greatCircleDistanceOutput = 41.676839;
}

- (void)tearDown {
    [super tearDown];
}

- (void)testGreatCircleDistance_ShouldReturnTheCorrectDistanceInKM {
    double greatCircleDistanceInput = [Location greatCircleDistanceFromLatitude:latitude longitud:longitude];
    NSString *roundInput = [NSString stringWithFormat:@"%.6f", greatCircleDistanceInput];
    NSString *roundOutput = [NSString stringWithFormat:@"%.6f", greatCircleDistanceOutput];
    XCTAssertEqualObjects(roundInput, roundOutput);
}

@end
