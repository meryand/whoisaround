//
//  CustomerListViewCell.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "CustomerListViewCell.h"

@implementation CustomerListViewCell

- (void)updateWithViewModel:(CustomerListViewModel *)viewModel {
    self.customerName.text = viewModel.customerName;
    self.customerID.text = viewModel.customerID;
    self.distanceInKM.text = viewModel.distanceInKM;
}

@end
