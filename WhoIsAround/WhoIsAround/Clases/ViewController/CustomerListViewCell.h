//
//  CustomerListViewCell.h
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerListViewModel.h"

@interface CustomerListViewCell : UITableViewCell

/**
 *  Customer Name View Label
 */
@property (weak, nonatomic) IBOutlet UILabel *customerName;

/**
 *  User ID View Label
 */
@property (weak, nonatomic) IBOutlet UILabel *customerID;

/**
 *  Customer distance in KM to the office
 */
@property (weak, nonatomic) IBOutlet UILabel *distanceInKM;

/**
 *  Set the viewModel used for the Customer model
 *
 *  @param viewModel a CustomerListViewModel
 */
- (void)updateWithViewModel:(CustomerListViewModel *)viewModel;

@end
