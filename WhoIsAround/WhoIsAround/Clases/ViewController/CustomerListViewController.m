//
//  CustomerListViewController.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "CustomerListViewController.h"
#import "CustomerListDataController.h"
#import "CustomerListViewCell.h"
#import "CustomerListViewModel.h"
#import "Customer.h"

static NSString *const customerCellIdentifier = @"customerCell";
static NSString *const customerJSONFileName = @"customers";

@interface CustomerListViewController ()
@property (nonatomic, strong) NSArray *customers;
@property (nonatomic, strong) CustomerListDataController *dataController;
@end

@implementation CustomerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.dataController = [CustomerListDataController new];
    self.customers = [self.dataController customersDataFromFile:customerJSONFileName];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.customers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomerListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:customerCellIdentifier forIndexPath:indexPath];
    CustomerListViewModel *viewModel = [CustomerListViewModel viewModelWithModel:self.customers[indexPath.row]];
    [cell updateWithViewModel:viewModel];
    return cell;
}

@end
