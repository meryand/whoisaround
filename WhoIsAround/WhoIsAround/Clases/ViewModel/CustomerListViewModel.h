//
//  CustomerListViewModel.h
//  WhoIsAround
//
//  Created by Ortega, Maria on 12/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Customer.h"

@interface CustomerListViewModel : NSObject

@property (strong, nonatomic) NSString *customerName;
@property (strong, nonatomic) NSString *customerID;
@property (strong, nonatomic) NSString *distanceInKM;

/**
 *  Creates a new instance
 *
 *  @param data model a Customer model
 *
 *  @return A new instance
 */
+ (instancetype)viewModelWithModel:(Customer *)model;

@end
