//
//  CustomerListViewModel.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 12/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "CustomerListViewModel.h"

@implementation CustomerListViewModel

+ (instancetype)viewModelWithModel:(Customer *)model {
    return [[self alloc] initWithModel:model];
}

- (instancetype)initWithModel:(Customer *)model {
    self = [super init];
    if (self) {
        _customerName = model.name;
        _customerID = [NSString stringWithFormat:@"%@",model.userID];
        _distanceInKM = [NSString stringWithFormat:@"%.2fKm",model.distance];
    }
    return self;
}

@end
