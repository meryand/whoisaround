//
//  Customer.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "Customer.h"
#import "Location.h"

@implementation Customer

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if ([dictionary isKindOfClass:[NSDictionary class]]) {
            [self setValuesForKeysWithDictionary:dictionary];
            self.distance = [self getDistanceToOffice];
        }
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    if ([key isEqualToString:@"name"]) {
        self.name = value;
    } else if ([key isEqualToString:@"user_id"]) {
        self.userID = value;
    } else if ([key isEqualToString:@"latitude"]) {
        self.latitude = value;
    } else if ([key isEqualToString:@"longitude"]) {
        self.longitude = value;
    }
}

- (double)getDistanceToOffice {
    return [Location greatCircleDistanceFromLatitude:[self.latitude doubleValue]
                                            longitud:[self.longitude doubleValue]];
}

@end
