//
//  Customer.h
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customer : NSObject

/**
 *  Customer Name
 */
@property (strong, nonatomic) NSString *name;

/**
 *  User Identification Number
 */
@property (strong, nonatomic) NSNumber *userID;

/**
 *  Customer Latitude
 */
@property (strong, nonatomic) NSString *latitude;

/**
 *  Customer Longitude
 */
@property (strong, nonatomic) NSString *longitude;

/**
 *  Customer distance to the office
 */
@property (assign, nonatomic) double distance;

/**
 *  The designated initializer
 *
 *  @param dictionary a dictionary
 *  @return an instance of model
 */
- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
