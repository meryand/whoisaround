//
//  CustomerListDataController.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "CustomerListDataController.h"
#import "JSONParser.h"
#import "Customer.h"

static NSString *const userIDKey = @"userID";
static double distance100KMKey = 100;

@implementation CustomerListDataController

- (NSArray *)customersDataFromFile:(NSString *)fileName {
    NSArray *customersArray = [JSONParser dataFromJsonForFileName:fileName];
    NSArray *customersArraySorted = [self sortArrayAscending:customersArray byKey:userIDKey];
    return [self getAllCustomerInArray:customersArraySorted withInDistance:[NSNumber numberWithDouble:distance100KMKey]];
}

- (NSArray *)sortArrayAscending:(NSArray *)customers byKey:(NSString *)key {
    NSMutableArray *sortArray = [NSMutableArray arrayWithArray:customers];
    [sortArray sortUsingDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:key ascending:YES], nil]];
    return sortArray.copy;
}

- (NSArray *)getAllCustomerInArray:(NSArray *)customers withInDistance:(NSNumber *)distanceKM {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(distance <= %@)",distanceKM];
    return [customers filteredArrayUsingPredicate:predicate];
}

@end
