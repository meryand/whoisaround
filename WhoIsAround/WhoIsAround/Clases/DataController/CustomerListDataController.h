//
//  CustomerListDataController.h
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerListDataController : NSObject

/**
 *  Return the customers array after beeing parsed, sorted and filtered
 *
 *  @param fileName the name of the json file
 *
 *  @return array customers array
 */
- (NSArray *)customersDataFromFile:(NSString *)fileName;

@end
