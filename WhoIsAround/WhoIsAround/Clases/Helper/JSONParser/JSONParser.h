//
//  JSONParser.h
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONParser : NSObject

/**
 *  JSON Parser froma a file
 *
 *  @param fileName the name of the json file
 *
 *  @return array of customer models parsed
 */
+ (NSArray *)dataFromJsonForFileName:(NSString *)fileName;

@end
