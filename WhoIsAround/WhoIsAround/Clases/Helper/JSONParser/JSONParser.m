//
//  JSONParser.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "JSONParser.h"
#import "Customer.h"

@implementation JSONParser

+ (NSArray *)dataFromJsonForFileName:(NSString *)fileName {
    NSError *jsonError;
    NSArray *jsonDataArray = [NSArray new];
    NSMutableArray *customersArray = [NSMutableArray new];

    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    jsonDataArray = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                                    options:kNilOptions
                                                      error:&jsonError];
    if (jsonDataArray != nil) {
        for (NSDictionary *dictionary in jsonDataArray) {
            Customer *customer = [[Customer alloc] initWithDictionary:dictionary];
            [customersArray addObject:customer];
        }
    }
    return customersArray.copy;
}

@end
