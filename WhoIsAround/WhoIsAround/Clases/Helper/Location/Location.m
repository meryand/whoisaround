//
//  Location.m
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "Location.h"

static double latitudeIntercomOffice = 53.3381985;
static double longitudIntercomOffice = -6.2592576;
static double earthRadiusinKm = 6371;

@implementation Location

+ (double)greatCircleDistanceFromLatitude:(double)latitude
                                 longitud:(double)longitud {
    double dLat = [self convertDegreesToRadians:(latitude - latitudeIntercomOffice)];
    double dLon = [self convertDegreesToRadians:(longitud - longitudIntercomOffice)];
    
    double a = (sin(dLat/2) * sin(dLat/2)) +
    cos([self convertDegreesToRadians:(latitudeIntercomOffice)]) * cos([self convertDegreesToRadians:(latitude)]) *
    (sin(dLon/2) * sin(dLon/2));
    
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
   
    double d = earthRadiusinKm * c;
    
    return d;
}

+ (double)convertDegreesToRadians:(double)degrees {
    return degrees * (M_PI/180);
}

@end
