//
//  Location.h
//  WhoIsAround
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

/**
 *  The Great Circle Distance Formula
 *
 *  @param latitude a dictionary
 *  @param latitude a longitud
 *
 *  @return double which is the shortest distance between two points on the surface of a sphere.
 */
+ (double)greatCircleDistanceFromLatitude:(double)latitude
                                 longitud:(double)longitud;

@end
